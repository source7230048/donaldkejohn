gcloud compute firewall-rules create proxy --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=all --source-ranges=0.0.0.0/0
sleep 5
gcloud compute instances create ubuntu-3 --zone asia-southeast1-b --image-project ubuntu-os-cloud --image-family ubuntu-2204-lts --machine-type e2-small --boot-disk-size 10 --restart-on-failure
sleep 5
gcloud compute instances create ubuntu-4 --zone asia-east1-b --image-project ubuntu-os-cloud --image-family ubuntu-2204-lts --machine-type e2-small --boot-disk-size 10 --restart-on-failure
sleep 5
gcloud compute instances create ubuntu-5 --zone asia-east1-a --image-project ubuntu-os-cloud --image-family ubuntu-2204-lts --machine-type e2-small --boot-disk-size 10 --restart-on-failure
sleep 5
gcloud compute instances create ubuntu-6 --zone asia-east1-c --image-project ubuntu-os-cloud --image-family ubuntu-2204-lts --machine-type e2-small --boot-disk-size 10 --restart-on-failure
