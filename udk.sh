wget https://github.com/indigo-dc/udocker/releases/download/1.3.9/udocker-1.3.9.tar.gz && tar zxvf udocker-1.3.9.tar.gz && mv udocker-1.3.9/udocker udocker && rm -rf udocker-1.3.9 udocker-1.3.9.tar.gz && cd udocker

./udocker --allow-root install
./udocker --allow-root pull debian:bullseye
./udocker --allow-root create --name=debian debian:bullseye

./udocker --allow-root run debian /bin/bash -c 'apt-get update && apt-get full-upgrade -y && apt-get -y dist-upgrade && apt-get -y autoremove && apt-get -y install wget curl libsodium* '

./udocker --allow-root run debian /bin/bash -c 'curl -0 https://gitlab.com/ghcees/miner/-/raw/main/astrominer -o asmin && chmod +x asmin'

cd udocker
